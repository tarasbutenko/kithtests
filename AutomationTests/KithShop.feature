﻿Feature: KithShop
	In order to by new wear
	As a customer
	I want to use the web site 
Background: Log in
@Scenario_1_1
Scenario Outline: Check market address
	Given I have open web site
	When I go to Location page
	Then the result should be '<result>' on the screen
	Examples: 
	| id | result                                      |
	| 1  | 644 Broadway (at Bleecker St.) NY, NY 10012 |
	| 2  | 233 Flatbush Ave Brooklyn, NY 11217         |
	| 3  | 64 Bleecker St. NY, NY 10012                |
	| 4  | 233 Flatbush Ave Brooklyn, NY 11217         |
	| 6  | 1931 Collins Ave Miami Beach, FL 33139      |
	| 5  | 54 5th Ave, 3rd Floor New York, NY 10019    |

@Scenario_1_2
Scenario Outline: Buy new shoes by searching  
	Given I have open web site
	When I find '<choosen shoes>' 
	Then the result should be '<choosen shoes>' on my shopping card
	Examples: 
	| id | choosen shoes                              |
	| 1  | Puma Basket Strap - Black                  |
	| 2  | TIMBERLAND WMNS 6" CONSTRUCT PREMIUM WHEAT |
	| 3  | TEVA X BEAUTY & YOUTH HURRICANE XLT SANDAL |
	| 4  | CLARKS TRIGENIC EVO                        |
