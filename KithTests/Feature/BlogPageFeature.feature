﻿Feature: BlogPageFeature

@Scenario_4_1
Scenario: Check blog page filters
	Given I am on home page
	When I go to "Blog" page
	And I select "Apparel" filter
	Then I can see "AIMÉ LEON DORE FALL 2017" in article title block

@Scenario_4_2
Scenario: Check blog page
	Given I am on home page
	When I go to "Blog" page
	Then I can see "ADIDAS ORIGINALS WMNS FALL '17, DELIVERY 1" in article title block