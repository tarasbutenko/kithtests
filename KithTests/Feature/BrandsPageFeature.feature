﻿Feature: BrandsPageFeature

@Scenario_6_2
Scenario: Check brands page list
	Given I am on home page
	When I go to "Brands" page
	Then I can see this brands in the page
	| Id | Letter | Name     |
	| 1  | A      | ADIDAS   |
	| 2  | C      | CONVERSE |
	| 3  | K      | KITH     |