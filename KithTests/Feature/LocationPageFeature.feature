﻿Feature: LocationPageFeature

@Scenario_5_2
	Scenario: Check market address
	Given I am on home page
	When I go to "Location" page
	Then I can see "337 Lafayette St. NY, NY 10012" in one of the blocks for addresses
	And I can see "233 Flatbush Ave Brooklyn, NY 11217" in one of the blocks for addresses
	And I can see "233 Flatbush Ave Brooklyn, NY 11217" in one of the blocks for addresses
	And I can see "54 5th Ave, 3rd Floor New York, NY 10019" in one of the blocks for addresses
	And I can see "1931 Collins Ave Miami Beach, FL 33139" in one of the blocks for addresses