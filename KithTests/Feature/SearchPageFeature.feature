﻿Feature: SearchPageFeature
	
@Scenario_3_1
Scenario Outline: Search and buy new shoes. 
	Given I am on home page
	When  I enter "<choosen shoes>" for the search to match the full name
	And I select first product from search result 
	And I click on buy now button 
	Then  I can see  "<choosen shoes>" in my shoping card
	Examples: 
	| choosen shoes                                         |
	| Clarks Trigenic Evo - Cola                            |
	| adidas Originals x Mastermind World EQT Ultra - Black |
	| Teva x Beauty & Youth Hurricane XLT Sandal - Green    |
	| Timberland WMNS 6" Construct Premium - Wheat          |

@Scenario_3_2
Scenario: Finding shoes using filter. 
	Given I am on footwear page
	And  I select "asics" and "13" filters
	And  I can see  "ASICS GEL-LYTE III" in searching results block
	When  I click to Clear all filter button
	Then  I can see  "FRONTEER AQUA SOLO" in product showing block
