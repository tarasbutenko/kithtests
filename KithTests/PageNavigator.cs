﻿using KithTests.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KithTests
{
    public static class PageNavigator
    {
        private static IWebDriver browser;
        private static Dictionary<string, KithPageObject> PageDictionary = new Dictionary<string, KithPageObject>
        {
            {"Location", new KithLocationPage(PageNavigator.browser) },
            {"Tracking", new KithTrackingPage(PageNavigator.browser) },
        };
        public static KithPageObject GetPageByName(string pageName, IWebDriver browser)
        {
            if (PageDictionary.ContainsKey(pageName))
            {
                PageNavigator.browser = browser;
                return PageDictionary[pageName];
            }
            else
            {
                throw new Exception($"Not such {pageName} page was found");
            }
        }
    }
}
