﻿using KithTests.Pages;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithAcountPage : KithPageObject
    {
        [FindsBy(How = How.TagName, Using = "h5")]
        TextBlock TextBoxSearch { get; set; }

        public KithAcountPage(IWebDriver browser):base(browser)
        {
        }
        public string GetAcountDetails()
        {
            return TextBoxSearch.Text;
        }
    }
}
