﻿using KithTests.Pages;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithBlogPage : KithPageObject
    {
        [FindsBy(How = How.XPath, Using = ".//h3[@class='article-card-title']")]
        IList<IWebElement> ArticleTitles { get; set; }

        [FindsBy(How = How.ClassName, Using = "taggers-list-item-main")]
        IList<IWebElement> FiltersName { get; set; }

        private Dictionary<string, IWebElement> Filters;
        public KithBlogPage(IWebDriver browser) : base(browser)
        {
            Filters = new Dictionary<string, IWebElement>();
        }
        public string GetArticleTitle(int articleNumber)
        {
            return ArticleTitles[articleNumber].Text;
        }
        public IWebElement GetFilterByName(string filterName)
        {
            if (filterName == "Apparel")
            {
                return FiltersName[3];
            }
            else
            {
                throw new Exception();
            }
        }
    }
}