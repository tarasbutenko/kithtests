﻿using KithTests.Pages;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithBrandsPage : KithPageObject
    {
        
        [FindsBy(How = How.XPath, Using = ".//*[@class='mb2 text-uppercase']")]
        IList<IWebElement> BrandsName { get; set; }

        public KithBrandsPage(IWebDriver browser) : base(browser) { }

        public IList<IWebElement> GetBrandsNames()
        {
            return BrandsName;
        }
    }
}
