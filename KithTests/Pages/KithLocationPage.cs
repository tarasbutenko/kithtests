﻿using KithTests.WebElements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithLocationPage : KithPageObject
    {
        [FindsBy(How = How.XPath, Using = ".//*[@class='locations']/div[1]/div/div[1]/*[@class='locations-content']/div[1]")]
        public IList<IWebElement> LocationsList { get; set; }

        public KithLocationPage(IWebDriver browser) : base(browser)
        {

        }
        public string GetLocationAddress(int position)
        {
            var element = LocationsList[position];
            return element.Text;
        }
    }
}
