﻿using KithTests.Pages;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithLookbookPage : KithPageObject
    {
        [FindsBy(How = How.XPath, Using = ".//h3[@class='article-card-title']")]
        IList<IWebElement> LookbooksTitles { get; set; }

        private Dictionary<string, IWebElement> Lookbooks;
        public KithLookbookPage(IWebDriver browser):base(browser)
        {
            Lookbooks = new Dictionary<string, IWebElement>();
        }

        public string GetLookbookTitle(int lookbookNumber)
        {
            return LookbooksTitles[lookbookNumber].Text;
        }
    }
}