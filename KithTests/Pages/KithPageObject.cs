﻿using HtmlElements.Elements;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithPageObject
    {


        public static IWebDriver driver;
        protected readonly string url = "https://kith.com/";
        [WebElements.FindsBy(How = How.XPath, Using = ".//ul[@class='ksplash-header-main-links -right']/div[2]/a")]
        Link LinkLocation { get; set; }

        [WebElements.FindsBy(How = How.XPath, Using = ".//ul[1]/li[4]/a[@class='site-footer-navlist-link']")]
        Link LinkLookbook { get; set; }

        [WebElements.FindsBy(How = How.XPath, Using = ".//ul[3]/li[1]/a[@class='site-footer-navlist-link']")]
        Link LinkTracking { get; set; }

        [WebElements.FindsBy(How = How.XPath, Using = ".//ul[1]/li[3]/a[@class='site-footer-navlist-link']")]
        Link LinkBlog { get; set; }

        [WebElements.FindsBy(How = How.XPath, Using = ".//ul[1]/li[2]/a[@class='site-footer-navlist-link']")]
        Link LinkBrands { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-main-link']//span")]
        Link LinkMan { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-cart-container-inner']/div[3]/a[1]")]
        Link LinkLogIn;

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-account-inputgroup']/input[1]")]
        TextBox TextBoxEmail { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-account-inputgroup']/input[2]")]
        TextBox TextBoxPassword { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='btn btn--full btn--small']")]
        TestButton ButtonSignIn;

        [FindsBy(How = How.XPath, Using = ".//div[@data-menu-order='1']/div[2]//*[@href='/collections/footwear/sneaker']")]
        Link LinkFootwear { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ajaxcart-product-name']")]
        IList<IWebElement> ShopingCardProducts { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-search-input js-ksplash-header-search-input snize-input-style']")]
        TextBox TextBoxSearch { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ksplash-header-logo']/a[@href='/']")]
        Link LinkToMain { get; set; }

        [FindsBy(How = How.XPath, Using = ".//div[@id='CartDrawer']//*[@class='icon icon-minus']")]
        TestButton DeleteFromShoppingCard { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='ajaxcart-product-name']")]

        IList<IWebElement> ShoppingCardItemsList { get; set; }
        //public IList<IWebElement> ShoppingCardItemsList;


        private Dictionary<string, ICustomWebElement> LinksToPage;
        public KithPageObject(IWebDriver browser)
        {
            driver = browser;
            var listTypes = new List<Type>();
            // listTypes.Add(typeof(ICustomWebElement));
            //  listTypes.Add(typeof(TestButton));
            //  listTypes.Add(typeof(Link));
            OpenQA.Selenium.Support.PageObjects.PageFactory.InitElements(browser, this, new DefaultPageObjectMemberDecorator(listTypes));
            LinksToPage = new Dictionary<string, ICustomWebElement>
            {
                {"Location", LinkLocation },
                {"Lookbook", LinkLookbook },
                { "Tracking", LinkTracking },
                { "Blog", LinkBlog },
                { "Brands", LinkBrands },
            };
            //listTypes.Add(typeof(TextBlock));
            OpenQA.Selenium.Support.PageObjects.PageFactory.InitElements(browser, this , new DefaultPageObjectMemberDecorator(listTypes));
        }

        public void LogIn(string email, string password)
        {
            LinkLogIn.Click();
            TextBoxEmail.SendKeys(email);
            TextBoxPassword.SendKeys(password);
            ButtonSignIn.Click();
        }

        public IList<IWebElement> GetListFromShopingCard()
        {
            return ShoppingCardItemsList;
            //return new List<IWebElement>(ShoppingCardItemsList);
        }




        public void DeleteElementsFromShoppingCard()
        {
            DeleteFromShoppingCard.Click();
            var element = driver.FindElement(By.XPath(".//*[@id='CartContainer']/p"));
        }
        public void FindProduct(string productName)
        {
            TextBoxSearch.SendKeys(productName);
            TextBoxSearch.SendKeys(Keys.Enter);
        }
        public void GoToPage(string pageName)
        {
                LinksToPage[pageName].Click();
        }
        public void GoToMain()
        {
            if (driver.Url != "https://kith.com/")
            {
                LinkToMain.Click();
            }
        }
        public void Close()
        {
            driver.Close();
        }
        public void GoToFootwear()
        {
            Thread.Sleep(100);
            new Actions(driver).MoveToElement(LinkMan).Build().Perform();
            Thread.Sleep(100);
            new Actions(driver).MoveToElement(LinkFootwear).Click().Build().Perform();
        }
    }
}
