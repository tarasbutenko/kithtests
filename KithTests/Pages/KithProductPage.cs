﻿using KithTests.WebElements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Text;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithProductPage : KithPageObject
    {
        [FindsBy(How = How.XPath, Using = ".//*[@id='AddToCartText']")]
        private TestButton ButtonBuyNow;

        public KithProductPage(IWebDriver browser) : base(browser)
        {
        }
        public void AddProductToShoppingCard()
        {
            ButtonBuyNow.Click();
        }
    }
}

