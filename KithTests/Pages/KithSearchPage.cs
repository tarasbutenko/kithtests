﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using KithTests.WebElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithSearchPage:KithPageObject
    {

        [FindsBy(How = How.XPath, Using = ".//div[@id='PageContainer']//ul[@class='nav-size ']/li/div/a")]
        public IList<IWebElement> SizeFiltersList { get; set; }

        [FindsBy(How = How.XPath, Using = ".//div[@id='PageContainer']//ul[@class='nav-brand ']/li/div/a")]
        IList<IWebElement> BrandFiltersList { get; set; }

        [FindsBy(How = How.XPath, Using = ".//span[@class='snize-overhidden']/span[@class='snize-title']")]
        public IList<IWebElement> SearchResultsList { get; set; }

        [FindsBy(How = How.XPath, Using = ".//div[@id='PageContainer']//*[@class='filter-group refine-header']//a")]
        Link LinkClearFilters { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@class='grid-uniform grid--center wide--grid--middle']/div//span")]
        public IList<IWebElement> FiltersSearchResultsList { get; set; }    

        public KithSearchPage(IWebDriver browser) : base(browser)
        {
        }
        public void SelectSizeFilter(string filter)
        {
            var element = SizeFiltersList.FirstOrDefault(e => e.GetAttribute("title") == $"Narrow selection to products matching tag productsize-{filter}");
            element.Click();
        }
        public void SelectBrandFilter(string filter)
        {
            var element = BrandFiltersList.FirstOrDefault(e => e.GetAttribute("title") == $"Narrow selection to products matching tag {filter}" );
            Thread.Sleep(1000);
            element.Click();
           
        }
        public void ClearFilters()
        {
            LinkClearFilters.Click();
        }
        public string GetNameFromSearchResult(int position)
        {
            var element = FiltersSearchResultsList[position];
            new Actions(driver).MoveToElement(element).Build().Perform();
            Thread.Sleep(100);
            return element.Text;
        }
        public void GetFirstElementFromSearchResult()
        {
            var element = SearchResultsList[0];
            element.Click();
        }
    }
}
