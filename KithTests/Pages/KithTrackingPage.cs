﻿using KithTests.Pages;
using KithTests.WebElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KithTests.Pages
{
    public class KithTrackingPage : KithPageObject
    {
        [FindsBy(How = How.ClassName, Using = "heading-main")]
        TextBlock PageTitle { get; set; }

        public KithTrackingPage(IWebDriver browser):base(browser)
        {
        }
        public string GetTitle()
        {
            return PageTitle.Text;
        }
    }
}
