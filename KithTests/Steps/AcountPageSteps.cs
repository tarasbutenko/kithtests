﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class AcountPageSteps
    {
        public static KithAcountPage kithAcountPage;

        [Then(@"I can see myaccount information")]
        public void ThenICanSeeMyaccountInformation()
        {
            var actual = kithAcountPage.GetAcountDetails();
            Assert.AreEqual(actual, "Taras Butenko");
        }
    }
}
