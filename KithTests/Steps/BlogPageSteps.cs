﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class TrackingPageSteps
    {
        public static KithTrackingPage kithTrackingPage;

        [Then(@"I can see ""(.*)"" in the title of the page")]
        public void ThenICanSeeInTheTitleOfThePage(string pageTitle)
        {
            var actual = kithTrackingPage.GetTitle();
            actual = actual.Replace("\r\n", " ");
            Assert.AreEqual(pageTitle, actual);
        }
    }
}
