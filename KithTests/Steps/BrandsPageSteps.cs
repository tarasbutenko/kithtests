﻿using KithTests.Pages;
using KithTests.ViewModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class BrandsPageSteps
    {
        public static KithBrandsPage kithBrandsPage;

        [Then(@"I can see this brands in the page")]
        public void ThenICanSeeThisBrandsInThePage(Table table)
        {
            var brands = table.CreateSet<BrandVM>().ToList();
            var actualBrands = kithBrandsPage.GetBrandsNames();
            Assert.AreEqual(brands[0].Name, actualBrands.ToArray()[2].Text);
            Assert.AreEqual(brands[1].Name, actualBrands.ToArray()[17].Text);
            Assert.AreEqual(brands[2].Name, actualBrands.ToArray()[34].Text);
        }
    }
}
