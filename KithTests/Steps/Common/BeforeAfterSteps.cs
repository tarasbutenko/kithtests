﻿using KithTests.Pages;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace KithTests.Steps
{
    [Binding]
    public static class BeforeAfterSteps
    {
        [BeforeTestRun]
        public static void GivenLogIn()
        {
            KithShopSteps.ngDriver = new ChromeDriver(Path.Combine(Environment.CurrentDirectory, @"KithTests\Tools\"));
            KithShopSteps.wait = new WebDriverWait(KithShopSteps.ngDriver, TimeSpan.FromSeconds(1));
            KithShopSteps.ngDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 4);
            KithShopSteps.ngDriver.Manage().Window.Maximize();
            KithShopSteps.ngDriver.Url = "https://kith.com/";
            KithShopSteps.kithPage = new KithMainPage(KithShopSteps.ngDriver);
            AcountPageSteps.kithAcountPage = new KithAcountPage(KithShopSteps.ngDriver);
            BrandsPageSteps.kithBrandsPage = new KithBrandsPage(KithShopSteps.ngDriver);
            BlogPageSteps.kithBlogPage = new KithBlogPage(KithShopSteps.ngDriver);
            LookbookPageSteps.kithLookbookPage = new KithLookbookPage(KithShopSteps.ngDriver);
            LocationPageSteps.kithLocationPage = new KithLocationPage(KithShopSteps.ngDriver);
            ProductPageSteps.kithProductPage = new KithProductPage(KithShopSteps.ngDriver);
            SearchPageSteps.kithSearchPage = new KithSearchPage(KithShopSteps.ngDriver);
            TrackingPageSteps.kithTrackingPage = new KithTrackingPage(KithShopSteps.ngDriver);
        }
            
        [AfterTestRun]
        public static void TeardownTest()
        {
            KithShopSteps.kithPage.Close();
        }

    }
}
