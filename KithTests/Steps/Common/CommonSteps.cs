﻿using KithTests.Pages;
using KithTests.ViewModel;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class KithShopSteps
    {

        public static IWebDriver ngDriver;
        public static WebDriverWait wait;
        public static KithMainPage kithPage;

        [Given(@"I am on home page")]
        public void GivenIAmOnHomePage()
        {
            kithPage.GoToMain();
        }

        [Given(@"I am on footwear page")]
        public void GivenIAmOnFootwearPage()
        {
            kithPage.GoToFootwear();
        }

        [When(@"I log in")]
        public void WhenILogIn()
        {
            kithPage.LogIn("taras.butenko1@gmail.com", "qwerty1");
        }

        [When(@"I go to ""(.*)"" page")]
        public void WhenIGoToLocationPage(string pageName)
        {
            kithPage.GoToPage(pageName);
        }

        [When(@"I enter ""(.*)"" for the search to match the full name")]
        public void WhenIEnterForTheSearchToMatchTheFullName(string bootsName)
        {
            kithPage.FindProduct(bootsName);
        }

        [Then(@"I can see  ""(.*)"" in my shoping card")]
        public void ThenICanSeeInMyShopingCard(string shoes)
        {
            var list = kithPage.GetListFromShopingCard();
            Thread.Sleep(1000);
            var actual = list[0].Text;
            Assert.AreEqual(shoes, actual);
            kithPage.DeleteElementsFromShoppingCard();
        }

        private static string GetTextFromFile(string filePath)
        {
            StreamReader file = new StreamReader(filePath);
            string line = file.ReadLine();
            file.Close();
            return line;
        }
    }
}
