﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class LocationPageSteps
    {
        public static KithLocationPage kithLocationPage;

        [Then(@"I can see ""(.*)"" in one of the blocks for addresses")]
        public void ThenICanSeeInOneOfTheBlocksForAddresses(string address)
        {
            var actual = kithLocationPage.GetLocationAddress(i++);
            actual = actual.Replace("\r\n", " ");
            Assert.AreEqual(address, actual);
        }
    }
}
