﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class LookbookPageSteps
    {
        public static KithLookbookPage kithLookbookPage;

        [Then(@"I can see ""(.*)"" as first lookbook title block")]
        public void ThenICanSeeAsFirstLookbookTitleBlock(string lookbookName)
        {
            var actual = kithLookbookPage.GetLookbookTitle(0);
            actual = actual.Replace("\r\n", " ");
            Assert.AreEqual(lookbookName, actual);
        }
    }
}
