﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class ProductPageSteps
    {
        public static KithProductPage kithProductPage;

        [When(@"I click on buy now button")]
        public void WhenIClickOnBuyNowButton()
        {
            kithProductPage.AddProductToShoppingCard();
        }
    }
}
