﻿using KithTests.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace KithTests.Steps
{
    [Binding]
    public class SearchPageSteps
    {
        public static KithSearchPage kithSearchPage;

        [Given(@"I select ""(.*)"" and ""(.*)"" filters")]
        public void GivenISelectAndFilters(string brandFilter, string sizeFilter)
        {
            kithSearchPage.SelectBrandFilter(brandFilter);
            kithSearchPage.SelectSizeFilter(sizeFilter);
        }

        [Given(@"I can see  ""(.*)"" in searching results block")]
        public void GivenICanSeeInSearchingResultsBlock(string shoes)
        {
            var actual = kithSearchPage.GetNameFromSearchResult(0);
            Assert.AreEqual(shoes, actual);
        }

        [When(@"I select first product from search result")]
        public void WhenISelectFirstProductFromSearchResult()
        {
            kithSearchPage.GetFirstElementFromSearchResult();
        }

        [When(@"I click to Clear all filter button")]
        public void WhenIClickToClearAllFilterButton()
        {
            kithSearchPage.ClearFilters();
        }

        [Then(@"I can see  ""(.*)"" in product showing block")]
        public void ThenICanSeeInProductShowingBlock(string shoes)
        {
            var actual = kithSearchPage.GetNameFromSearchResult(0);
            Assert.AreEqual(shoes, actual);
        }
    }
}
