﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KithTests.ViewModel
{
    public class BrandVM
    {
        public int Id {get;set;}
        public string Letter {get;set;}
        public string Name {get;set;}

    }
}
