﻿using OpenQA.Selenium;
using OpenQA.Selenium.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KithTests.WebElements
{
    public interface ICustomWebElement : IWebElement, IWrapsElement
    {
        new IWebElement WrappedElement { get; set; }
    }
}
