﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace KithTests.WebElements
{
    class WebElementListProxy<T> : WebDriverObjectProxy where T : IWebElement
    { 
        private List<T> collection = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebElementListProxy"/> class.
        /// </summary>
        /// <param name="typeToBeProxied">The <see cref="Type"/> of object for which to create a proxy.</param>
        /// <param name="locator">The <see cref="IElementLocator"/> implementation that
        /// determines how elements are located.</param>
        /// <param name="bys">The list of methods by which to search for the elements.</param>
        /// <param name="cache"><see langword="true"/> to cache the lookup to the element; otherwise, <see langword="false"/>.</param>
        private WebElementListProxy(Type typeToBeProxied, IElementLocator locator, IEnumerable<By> bys, bool cache)
            : base(typeToBeProxied, locator, bys, cache)
        {
        }

        /// <summary>
        /// Gets the list of IWebElement objects this proxy represents, returning a cached one if requested.
        /// </summary>
        private List<T> GetElementList()
        {
            if (!this.Cache || this.collection == null)
            {
                collection = new List<T>();
                List<T> temp = new List<T>();
                if (typeof(T) == typeof(IWebElement))
                {

                    foreach (IWebElement x in this.Locator.LocateElements(this.Bys))
                    {
                        temp.Add((T)x);
                    }                    
                }
                else
                {
                    foreach (IWebElement x in this.Locator.LocateElements(this.Bys))
                    {
                        var tempVar = (T)(typeof(T).GetConstructors()[0].Invoke(null));
                        var tempArr = tempVar.GetType().GetMember("WrappedElement");
                        if (tempArr.Length >0)
                        {
                            var tempField = tempArr[0];
                            FieldInfo field;
                            PropertyInfo property;
                            field = (tempField as FieldInfo);
                            property = (tempField as PropertyInfo);
                            if (field != null)
                            {
                                field.SetValue(tempVar, x);
                            } else if (property != null)
                            {
                                property.SetValue(tempVar, x);
                            }
                            temp.Add(tempVar);
                        }
                       
                    }
                    
                }
                this.collection.AddRange(temp);
            }

            return this.collection;
        }

        /// <summary>
        /// Creates an object used to proxy calls to properties and methods of the
        /// list of <see cref="IWebElement"/> objects.
        /// </summary>
        /// <param name="classToProxy">The <see cref="Type"/> of object for which to create a proxy.</param>
        /// <param name="locator">The <see cref="IElementLocator"/> implementation that
        /// determines how elements are located.</param>
        /// <param name="bys">The list of methods by which to search for the elements.</param>
        /// <param name="cacheLookups"><see langword="true"/> to cache the lookup to the
        /// element; otherwise, <see langword="false"/>.</param>
        /// <returns>An object used to proxy calls to properties and methods of the
        /// list of <see cref="IWebElement"/> objects.</returns>
        public static object CreateProxy(Type classToProxy, IElementLocator locator, IEnumerable<By> bys, bool cacheLookups)
        {
            return new WebElementListProxy<T>(classToProxy, locator, bys, cacheLookups).GetTransparentProxy();
        }

        /// <summary>
        /// Invokes the method that is specified in the provided <see cref="IMessage"/> on the
        /// object that is represented by the current instance.
        /// </summary>
        /// <param name="msg">An <see cref="IMessage"/> that contains an <see cref="IDictionary"/>  of
        /// information about the method call. </param>
        /// <returns>The message returned by the invoked method, containing the return value and any
        /// out or ref parameters.</returns>
        public override IMessage Invoke(IMessage msg)
        {
            var elements = this.GetElementList();
            return WebDriverObjectProxy.InvokeMethod(msg as IMethodCallMessage, elements);
        }
    }
}